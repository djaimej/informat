import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class DataService {

  url:String = 'http://93.189.89.61:3000/api';

  constructor(private httpClient:HttpClient) { }

  public getData() {
    return this.httpClient.get(`${this.url}/municipality/list`);
  }

  public sendMessage(message) {
    let params = JSON.stringify(message);
    let headers = new HttpHeaders().set('Content-Type', 'Application/json');

    return this.httpClient.post(`${this.url}/alert`, params, {headers: headers});
  }
}
