import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  { path: '', redirectTo: 'index', pathMatch: 'full' },
  { path: 'index', loadChildren: () => import('./index/index.module').then( m => m.IndexPageModule)},
  { path: 'detail', loadChildren: './detail/detail.module#DetailPageModule' },
  { path: 'home', loadChildren: './home/home.module#HomePageModule' },
  { path: 'question', loadChildren: './question/question.module#QuestionPageModule' }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
